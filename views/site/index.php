<?php

/* @var $this yii\web\View */

$this->title = 'Yii';
?>
<section class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="header__text">
                    <p>Интернет-решения и продающие сайты</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about about_yii2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h1>Интернет ресурс на движке Yii2</h1>
                <p>
                    Et dui, tincidunt ac nascetur. Et feugiat eu, et augue elementum,
                    amet. Morbi adipiscing in purus diam egestas placerat orci. Mauris
                    quis consectetur et consequat et habitant. Aenean amet cras ornare
                    cras odio nisi, nisl.
                </p>
            </div>
            <div class="col-lg-6 about__img-yii2">
                <img src="images/dest/yii_logo_light.png" alt="Yii2 logo" />
            </div>
        </div>
    </div>
</section>

<section class="advantages advantages_yii2">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <h1>Преимущества разработки на Yii2</h1>
                <ul>
                    <li>Гибкая настройка под ваши задачи</li>
                    <li>Высокая отказоустойчивость ПО</li>
                    <li>Быстрая скорость работы</li>
                    <li>Безопастность ваших данных</li>
                    <li>Возможность постоянно расширять функционал</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="lates_works">
    <div class="container-fluid">
        <div class="row">
            <h1>Примеры разработанных веб-ресурсов</h1>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="lates_works__card">
                            <img
                                    src="images/dest/work1.jpg"
                                    alt=""
                                    class="lates_works__img"
                            />
                            <div class="lates_works__overlay">
                                <p class="lates_works__site">Site</p>
                                <p class="lates_works__desc">Desc</p>
                            </div>
                            <div class="lates_works__overlay_button">
                                <button class="btn overlay_btn">Button</button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="lates_works__card">
                            <img
                                    src="images/dest/work2.jpg"
                                    alt=""
                                    class="lates_works__img"
                            />
                            <div class="lates_works__overlay">
                                <p class="lates_works__site">Site</p>
                                <p class="lates_works__desc">Desc</p>
                            </div>
                            <div class="lates_works__overlay_button">
                                <button class="btn overlay_btn">Button</button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="lates_works__card">
                            <img
                                    src="images/dest/work3.jpg"
                                    alt=""
                                    class="lates_works__img"
                            />
                            <div class="lates_works__overlay">
                                <p class="lates_works__site">Site</p>
                                <p class="lates_works__desc">Desc</p>
                            </div>
                            <div class="lates_works__overlay_button">
                                <button class="btn overlay_btn">Button</button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="lates_works__card">
                            <img
                                    src="images/dest/work1.jpg"
                                    alt=""
                                    class="lates_works__img"
                            />
                            <div class="lates_works__overlay">
                                <p class="lates_works__site">Site</p>
                                <p class="lates_works__desc">Desc</p>
                            </div>
                            <div class="lates_works__overlay_button">
                                <button class="btn overlay_btn">Button</button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="lates_works__card">
                            <img
                                    src="images/dest/work2.jpg"
                                    alt=""
                                    class="lates_works__img"
                            />
                            <div class="lates_works__overlay">
                                <p class="lates_works__site">Site</p>
                                <p class="lates_works__desc">Desc</p>
                            </div>
                            <div class="lates_works__overlay_button">
                                <button class="btn overlay_btn">Button</button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="lates_works__card">
                            <img
                                    src="images/dest/work3.jpg"
                                    alt=""
                                    class="lates_works__img"
                            />
                            <div class="lates_works__overlay">
                                <p class="lates_works__site">Site</p>
                                <p class="lates_works__desc">Desc</p>
                            </div>
                            <div class="lates_works__overlay_button">
                                <button class="btn overlay_btn">Button</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Scrollbar -->
                <div class="swiper-scrollbar"></div>
            </div>
        </div>
    </div>
</section>

<section class="price_plans">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <h1>Выбор тарифного плана</h1>
                <h2>Небольшое объяснение по тарифным планам</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-4">
                <div class="price_card">
                    <div class="price_card__header">
                        <div class="price_title">Старт</div>
                        <div class="price_total">0$</div>
                        <div class="price_desc">безлимитный</div>
                    </div>
                    <div class="price_card__footer">
                        <ul>
                            <li>1 Пользователь</li>
                            <li>1 сайт</li>
                            <li>E-mail поддержка</li>
                        </ul>
                        <button class="btn price_btn">BUTTON</button>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-4">
                <div class="price_card">
                    <div class="price_card__header">
                        <div class="price_title">Старт</div>
                        <div class="price_total">0$</div>
                        <div class="price_desc">безлимитный</div>
                    </div>
                    <div class="price_card__footer">
                        <ul>
                            <li>1 Пользователь</li>
                            <li>1 сайт</li>
                            <li>E-mail поддержка</li>
                        </ul>
                        <button class="btn price_btn">BUTTON</button>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-4">
                <div class="price_card">
                    <div class="price_card__header">
                        <div class="price_title">Старт</div>
                        <div class="price_total">0$</div>
                        <div class="price_desc">безлимитный</div>
                    </div>
                    <div class="price_card__footer">
                        <ul>
                            <li>1 Пользователь</li>
                            <li>1 сайт</li>
                            <li>E-mail поддержка</li>
                        </ul>
                        <button class="btn price_btn">BUTTON</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
